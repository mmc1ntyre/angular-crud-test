(function () {
    var SignInCtrl = angular.module('lms').controller(
        'SignInController',
        ['$scope', '$state', 'api', '$rootScope', function ($scope, $state, api, $rootScope) {
            $scope.isAuthenticationError = api.isAuthenticationError;
            $rootScope.pageSignature = "signin";

            $scope.attemptLogin = function (email, password) {
                api.logIn(email, password).then(
                    function(user) { $state.go('dashboard'); }
                );
            };
        }]
    );
}());
