// Factory

(function(){

    'use strict';

    var businessesFactory = function($q, api) {
        var that = {};

        var index = that.index = function(page, perPage) {
            return api.http({
                url: 'businesses',
                method: 'get',
                params: {'page': page, 'per_page': perPage}
            });
        };

        var get = that.get = function (businessId) {
            return api.http({
                url: 'businesses/' + businessId,
                method: 'get'
            });
        };

        var create = that.create = function (business) {
            var newBusinessType = Object.assign({}, business);
            return api.http({
                url: 'businesses',
                method: 'post',
                data: newBusinessType
            })
        };

        var update = that.update = function (businessType) {
            var newBusinessType = Object.assign({}, businessType);
            return api.http({
                url: 'businesses/' + business.id,
                method: 'patch',
                data: newBusiness
            });
        };

        var remove = that.remove = function (businessId) {
            return api.http({
                url: 'businesses/' + businessId,
                method: 'delete'
            })
        }

        return that;
    };

    angular.module('lms').factory('businessesFactory',['$q', 'api', businessesFactory]);

}());