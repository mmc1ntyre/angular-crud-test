;(function () {
    'use strict';

    angular.module('lms').factory('api', ['$http', '$q', '$location', 'apiUrl', 'apiPort', 'storageEngine', function ($http, $q, $location, apiUrl, apiPort, storageEngine) {
        var that = {};

        /* public */

        /**
         * @return Promise - on success, 'unwraps' the `data` property.
         */
        var http = that.http = function(options) {
            options = options || {};
            options.method = options.method || 'get';
            options.url = toFullEndPoint(options.url);
            options.headers = {
                Authorization: 'Bearer ' + getAuthenticationToken()
            };

            return $http(options).then(function(response) {
                return response.data;
            }, function(errorResponse) {
                if (($location.path() !== '/') && ($location.path() !== '/register') && (errorResponse.status === 401)) {
                    $location.path('/');
                } else {
                    return errorResponse;
                }
            });
        };

        var isLoggedIn = that.isLoggedIn = function () {
            return !! getCurrentUser();
        };

        var getCurrentUser = that.getCurrentUser = function () {
            return storageEngine.getItem('user');
        };

        var logIn = that.logIn = function (email, password, callback) {
            return $http({
                url: toFullEndPoint('sessions'),
                data: {email: email, password: password},
                method: 'post'
            }).then(
                function (response) {
                    var bearer = response ? response.headers('bearer') : null;
                    setAuthenticationToken(bearer);
                    setUser(response.data);
                    return response.data;
                },
                function (errorResponse) {
                    logOut();
                    authenticationError = true;
                    return $q.reject(errorResponse);
                }
            );
        };

        var logOut = that.logOut = function () {
            storageEngine.removeItem('user');
            storageEngine.removeItem('bearer');
            authenticationError = null;
        };

        var isAuthenticationError = that.isAuthenticationError = function() {
            return authenticationError;
        };

        var getAuthenticationToken = that.getAuthenticationToken = function () {
            return storageEngine.getItem('bearer');
        };

        var toFullEndPoint = that.toFullEndPoint = function(relativeEndpoint) {
            return apiUrl + ':' + apiPort + '/api/' + relativeEndpoint;
        };

        /* private */
        var authenticationError = false;

        var setAuthenticationToken = function (token) {
            storageEngine.setItem('bearer', token);
        };

        var setUser = function (user) {
            if (!user) {
                authenticationError = true;
            } else {
                storageEngine.setItem('user', user);
                authenticationError = false;
            }
        };

        return that;
    }]);
}());
