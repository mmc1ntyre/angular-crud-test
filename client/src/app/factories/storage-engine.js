;(function () {
    'use strict';

    var storageEngineFactory = function () {
        var items = {},

            getItem = function (name) {
                if (window.sessionStorage) {
                    var jsonString = sessionStorage.getItem(name);
                    return !! jsonString ? JSON.parse(jsonString) : null;
                } else {
                    return (name in items) ? items[name] : null;
                }
            },

            setItem = function (name, value) {
                if (window.sessionStorage) {
                    return sessionStorage.setItem(name, JSON.stringify(value));
                } else {
                    items[name] = value;
                }
            },

            removeItem = function (name) {
                if (window.sessionStorage) {
                    return sessionStorage.removeItem(name);
                } else {
                    delete items[name];
                }
            };
        return {
            getItem: getItem,
            setItem: setItem,
            removeItem: removeItem
        };
    };

    angular.module('lms').factory('storageEngine', storageEngineFactory);
}());