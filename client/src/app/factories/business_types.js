// Factory

(function(){

    'use strict';

    var businessTypesFactory = function($q, api) {
        var that = {};

        var index = that.index = function () {
            return api.http({
                url: 'business_types',
                method: 'get'
            });
        };

        var get = that.get = function (businessTypeId) {
            return api.http({
                url: 'business_types/' + businessTypeId,
                method: 'get'
            });
        };

        var create = that.create = function (businessType) {
            var newBusinessType = Object.assign({}, businessType);
            return api.http({
                url: 'business_types',
                method: 'post',
                data: newBusinessType
            })
        };

        var update = that.update = function (businessType) {
            var newBusinessType = Object.assign({}, businessType);
            return api.http({
                url: 'business_types/' + businessType.id,
                method: 'patch',
                data: newBusinessType
            });
        };

        var remove = that.remove = function (businessTypeId) {
            return api.http({
                url: 'business_types/' + businessTypeId,
                method: 'delete'
            })
        }

        return that;
    };

    angular.module('lms').factory('businessTypesFactory',['$q', 'api', businessTypesFactory]);

}());