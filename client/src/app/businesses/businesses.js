// Controller

(function() {
   angular.module('lms').controller(
       'businessesController',
       ['$scope', 'businessesFactory', '$rootScope', function($scope, businessesFactory)
       {

           var that = $scope;
           var init = function() {

               var that = $scope;
               that.q = [];
               that.businesses = [];
               that.page = 1;
               that.perPage = 5;
               getResultsPage(that.page, that.perPage);
           };

           var getResultsPage = that.getResultsPage = function(page, perPage) {
               var that = $scope;

               businessesFactory.index(page, perPage).then(
                   function(response) {
                       if (response.status == undefined) {
                           that.businesses = response;
                       } else {
                           that.businesses = [];
                       }
                   }
               )

           };

           var nextPage = that.nextPage = function() {
               var that = $scope;
               var nextPage = that.page + 1;
               getResultsPage(that.page + 1, that.perPage);
           };

           var prevPage = that.prevPage = function() {
               var that = $scope;
               var nextPage = that.page - 1;
               if (nextPage < 1) {
                   nextPage = 1;
               };
               getResultsPage(nextPage, that.perPage);
           };


           init();

       }



       ]
   );
}());