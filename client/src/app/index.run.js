(function() {
  'use strict';

  angular
    .module('lms')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
