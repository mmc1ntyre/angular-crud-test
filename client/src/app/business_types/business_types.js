// Controller

(function() {
   angular.module('lms').controller(
       'businessTypesController',
       ['$scope', 'businessTypesFactory', '$rootScope', function($scope, businessTypesFactory){
           var init = function() {

               var that = $scope;

               that.businessTypes = [];
               businessTypesFactory.index().then(
                   function(response) {
                       if (response.status == undefined) {
                           that.businessTypes = response;
                       } else {
                           that.businessTypes = [];
                       }
                   }
               );
           };
           init();

           $scope.addBusinessType = function() {

               var that = $scope;

               businessTypesFactory.create(that.newBusinessType).then(
                   function(response) {
                       // response.status is undefined if call was successful
                       if (response.status == undefined) {
                         that.businessTypes.push(response);
                         that.newBusinessType = null;
                       }
                   }
               );
           };

           $scope.removeBusinessType = function(item) {
               var that = $scope;

               businessTypesFactory.remove(that.businessTypes[item].id).then(
                   function(response) {
                       if (response.status == undefined) {
                           that.businessTypes.splice(item, 1);
                       }
                   }
               )
           }

       }]
   );
}());