(function() {
  'use strict';

  angular
    .module('lms', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ui.router',
      'toastr',
      'ng-token-auth',
      'angular-toArrayFilter'
    ]);

})();
