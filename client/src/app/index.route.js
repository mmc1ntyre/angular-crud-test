//(function() {
//  'use strict';
//
//  angular
//    .module('lms')
//    .config(routerConfig);
//
//  /** @ngInject */
//  function routerConfig($stateProvider, $urlRouterProvider) {
//    $stateProvider
//      .state('home', {
//        url: '/',
//        templateUrl: 'app/main/main.html',
//        controller: 'MainController',
//        controllerAs: 'main'
//      })
//      .state('business_types', {
//        url: '/business_types',
//        templateUrl: 'app/components/business_types/business_types.html',
//        controller: 'businessTypesController',
//        controllerAs: 'businessType'
//      });
//
//    $urlRouterProvider.otherwise('/');
//  }
//
//})();

(function () {
    angular.module('lms').config(
        ['$stateProvider', '$urlRouterProvider', '$authProvider', 'componentPathPrefix', function ($stateProvider, $urlRouterProvider, $authProvider, componentPathPrefix) {

        $authProvider.configure({
            apiUrl: '',
            authProviderPaths: {
              facebook: '/auth/facebook'
            }
        });

        $stateProvider
            .state("main", {
                url: "/",
                templateUrl: "app/signin/login.html"
                //controller: "SignInController"
            })
            .state('business_types', {
                    url: '/business_types',
                    templateUrl: 'app/business_types/business_types.html',
                    controller: 'businessTypesController',
                    controllerAs: 'businessType'
                  })
            .state('businesses', {
                url: '/businesses',
                templateUrl: 'app/businesses/businesses_index.html',
                controller: 'businessesController',
                controllerAs: 'businesses'
            })
            //.state("register", {
            //    url: "/register",
            //    templateUrl: "app/register/landing.html"
            //})
            //.state("main.password_reset", {
            //    url: "users/password_reset",
            //    templateUrl: "app/users/password_reset.html",
            //    controller: "PasswordsCtrl"
            //})
            //.state("main.password_update", {
            //    url: "users/password_update",
            //    templateUrl: "app/users/password_update.html",
            //    controller: "UsersCtrl"
            //})
            //.state("main.register", {
            //    url: "users/register",
            //    templateUrl: "app/users/register.html",
            //    controller: "UsersCtrl"
            //})
            //.state("main.log_in", {
            //  url: "users/log_in",
            //  templateUrl: "app/users/log_in.html",
            //  controller: "UsersCtrl"
            //})
            //.state('dashboard', {
            //  url: "/dashboard",
            //  templateUrl: "app/dashboard/dashboard.html"
            //  //controller: "DashboardController"
            //})
            //.state('account', {
            //  url: '/account',
            //  templateUrl: "app/account/account.html"
            //  //controller: 'AccountController'
            //})
            //.state('explore', {
            //  url: '/explore',
            //  templateUrl: 'app/explore/explore.html',
            //  controller: 'ExploreController'
            //})
            //.state('projectsSearch', {
            //    url: '/projects/search/:category',
            //    templateUrl: 'app/explore/category.html'
            //})
            .state('logout', {
              url: '/logout',
              templateUrl: 'app/logout/logout.html',
              controller: 'LogoutController'
            })
            //.state('projects', {
            //  url: '/projects',
            //  templateUrl: 'app/projects/landing/projects.html'
            //  //controller: 'ProjectLandingController'
            //})
            //.state('projectsAdd', {
            //  url: '/projects/add',
            //  templateUrl: 'app/projects/create/create-project.html'
            //  //controller: 'CreateProjectController'
            //})
            //.state('projectForms', {
            //  url: '/projects/:id/forms',
            //  templateUrl: 'app/projects/forms/forms.html'
            //  //controller: 'ProjectFormsController'
            //})
            //.state('projectFormsAdd', {
            //  url: '/projects/:id/forms/add',
            //  templateUrl: 'app/projects/forms/create-form.html'
            //  //controller: 'CreateProjectFormController'
            //})
            //.state('projectFormsDetail', {
            //  url: '/projects/:id/forms/:form',
            //  templateUrl: 'app/projects/form-detail/form.html',
            //  controller: 'ProjectFormDetailController'
            //})
            //.state('projectData', {
            //  url: '/projects/:id/data',
            //  templateUrl: 'app/projects/data/data.html'
            //  //controller: 'ProjectDataController'
            //})
            //.state('projectFormClone', {
            //  url: '/projects/:id/forms/:form/clone',
            //  templateUrl: 'app/projects/forms/clone-form.html',
            //  controller: 'ProjectFormCloneController'
            //})
            //.state('projectFormEdit', {
            //  url: '/projects/:id/forms/:form_id/edit',
            //  templateUrl: 'app/projects/forms/edit-form.html'
            //  //controller: 'ProjectFormEditController'
            //})
            //.state('projectSettings', {
            //  url: '/projects/:id/settings',
            //  templateUrl: 'app/projects/edit/edit-project.html'
            //  //controller: 'EditProjectController'
            //})
            //.state('projectFormSectionsCreate', {
            //  url: '/projects/:id/forms/:form_id/sections/create',
            //  templateUrl: 'app/projects/sections/create-section.html',
            //  controller: 'CreateProjectFormSectionController'
            //})
            //.state('projectSectionsEdit', {
            //  url: '/projects/:id/forms/:form_id/sections/:section_id',
            //  templateUrl: 'app/projects/sections/edit-section.html',
            //  controller: 'ProjectFormSectionEditController'
            //})
            //.state('projectAnalyses', {
            //  url: '/projects/:id/analyses',
            //  templateUrl: 'app/projects/analyses/analyses.html'
            //  //controller: 'ProjectAnalysesController'
            //})
            //.state('projectAnalysisAdd', {
            //  url: '/projects/:id/analyses/add',
            //  templateUrl: 'app/projects/analyses/create-analysis.html'
            //  //controller: 'CreateProjectAnalysisController'
            //})
            //.state('projectAnalysisEdit', {
            //  url: '/projects/:id/analyses/:analysis_id/edit',
            //  templateUrl: 'app/projects/analyses/edit-analysis.html'
            //  //controller: 'EditProjectAnalysisController'
            //});

        //$urlRouterProvider.when('/projects/:id', '/projects/:id/forms');
        $urlRouterProvider.otherwise('/');
    }]);
}());

