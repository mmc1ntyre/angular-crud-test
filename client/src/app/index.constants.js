/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('lms')
    .constant('componentPathPrefix','/assets/components/')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('apiUrl', 'http://localhost')
    .constant('apiPort', 3000)

})();
