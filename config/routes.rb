Rails.application.routes.draw do
  namespace :api, defaults: {format: :json} do
    resources :business_types
    resources :businesses
  end
  get "/api/businesses/size" => "api/businesses#business_count"
end
