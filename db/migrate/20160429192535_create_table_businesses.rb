class CreateTableBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :name, null: false
      t.string :industry, null: false
      t.string :continent, null: false
      t.string :country, null: false
      t.float  :market_value
      t.float  :profits
      t.float  :assets
      t.integer :rank
      t.string :url
      t.datetime :deleted_at
    end
  end
end
