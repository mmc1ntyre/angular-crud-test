class BusinessTypes < ActiveRecord::Migration
  def change
    create_table :business_types do |t|
      t.string :name, null: false
      t.datetime :deleted_at
    end
  end
end
