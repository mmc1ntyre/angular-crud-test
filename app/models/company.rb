class Company < ActiveRecord::Base

  belongs_to :industry
  accepts_nested_attributes_for :industry

end