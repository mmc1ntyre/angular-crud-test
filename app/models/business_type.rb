class BusinessType < ActiveRecord::Base

  validates_uniqueness_of :name, :scope => :deleted_at
  validates_presence_of :name

end