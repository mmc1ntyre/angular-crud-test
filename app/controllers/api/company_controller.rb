class CompanyController < ApplicationController

  def index
    @companies = Company.paginate(:page => params[:page], :per_page => 25)
    if @companies
      render json: @companies, status: 200
    else
      render nothing: true, status: 404
    end
  end

  def create
    @company = Company.create!(company_params)
    render json: @company, status: 200
  end

  def show
    @company = Company.find(params[:id])
    if @company
      render json: @company, status: 200
    else
      render nothing: true, status: 404
    end
  end

  def delete
    @company = Company.find(params[:id])
    if @company.destroy
      render json: @company, status: 200
    else
      render nothing: true, status: 404
    end
  end

  def update
    @company = Company.find(params[:id])
    if @company.update_attributes(company_params)
      render json: @company, status: 200
    else
      render nothing: true, status: 404
    end

  end

  private

    def company_params
      params.permit(:id, :name, :sector, :industry, :continent, :country, :market_value, :profits, :assets, :rank, :url)
    end

end