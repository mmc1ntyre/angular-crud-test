class Api::BusinessesController < ApplicationController

  def index
    @businesses = Business.where(:deleted_at => nil).paginate(:page => params[:page], :per_page => 5)
    render json: @businesses
  end

  def create
    @business = Business.create!(:name => params[:name])
    render json: @business
  end

  def destroy
    @business = Business.find(params[:id])
    @business.update(:deleted_at => Time.now())
    render json: @business
  end

end