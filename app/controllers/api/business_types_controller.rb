class Api::BusinessTypesController < ApplicationController

  def index
    @business_types = BusinessType.where(:deleted_at => nil).uniq!
    render json: @business_types
  end

  def create
    @business_type = BusinessType.create!(:name => params[:name])
    render json: @business_type
  end

  def destroy
    @business_type = BusinessType.find(params[:id])
    @business_type.update(:deleted_at => Time.now())
    render json: @business_type
  end

end