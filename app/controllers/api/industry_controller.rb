class IndustryController < ApplicationController

  def index
    @industries = Industry.paginate(:page => params[:page], :per_page => 25)
    if @industries
      render json: @industries, status: 200
    else
      render nothing: true, status: 404
    end
  end

  def create
    @industry = Industry.create!(industry_params)
    render json: @industry, status: 200
  end

  def show
    @industry = Industry.find(params[:id])
    if @industry
      render json: @industry, status: 200
    else
      render nothing: true, status: 404
    end
  end

  def delete
    @industry = Industry.find(params[:id])
    if @industry.destroy
      render json: @industry, status: 200
    else
      render nothing: true, status: 404
    end
  end

  def update
    @industry = Industry.find(params[:id])
    if @industry.update_attributes(industry_params)
      render json: @industry, status: 200
    else
      render nothing: true, status: 404
    end

  end

  private

    def industry_params
      params.permit(:id, :name)
    end

end