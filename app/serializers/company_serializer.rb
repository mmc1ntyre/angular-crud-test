class CompanySerializer < ApplicationSerializer

  attribute :name, :sector, :industry, :continent, :country, :market_value, :profits, :assets, :rank, :url

  def industry
    return {'Id' => object.industry.id, 'Name' => object.industry.name}
  end

end