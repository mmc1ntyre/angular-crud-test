== README

The purpose of the project is to test potential new team members.
Use the following information to create an Angular front-end and a Rails-Api backend CRUD app.

1. Make sure you have ruby and nodejs installed
2. Clone the project
3. In the project root:
```
bundle install
```
4. In the client directory
``` npm install
    bower install
```

5. Create and seed your db

    - From project root run ```rake db:create```
    - Then run ```rake db:seed```

6. To run the project have 2 terminal windows open

    - cd into client - term 1

    - cd into the project root - term 2

    - From the project root run ```rails s```

    - From the client run ```gulp server```

ASSIGNMENT
==========

Goal:  Create a CRUD application

1.  Create an index page for Company.

2.  Create a page to add a New Company.

3.  Create a page to edit an Existing Company.

4.  On the index page...add actions next to each record to Edit and Remove record (Remove record should prompt with a
    warning)

5.  Add paging functionality to the index page.  You will have to modify company_controller.rb.

6.  Add filtering functionality to the index page.  Again, this will also require an API change.


Please do assignment in the order given.